#~/movie-bag/tests/test_signup.py

import unittest
import json
from app import app



class ApiTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()


    def test_post_propietario(self):
        # Given
        payload = json.dumps({
        "nombre":"Francisco Perez",
        "rfc": "PEGF920102HCD"
        })

        # When
        response = self.app.post('/propietario', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(int, type(response.json['id']))
        self.assertEqual(200, response.status_code)

    def test_get_propietario(self):

        response = self.app.get('/propietario')

        # Then
        self.assertEqual(int, type(response.json['propietarios'][0]['id']))
        self.assertEqual(200, response.status_code)

    def test_post_tienda(self):
        # Given
        payload = json.dumps({
        "marca":"oxxo",
        "direccion":"Impulsora, hacienda #34, Nezahualcoyotl, Edo. Mexico",
        "propietario_id":1
        })
        # When
        response = self.app.post('/tienda', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(int, type(response.json['id']))
        self.assertEqual(200, response.status_code)

    def test_get_tienda(self):

        # When
        response = self.app.get('/tienda')

        # Then
        self.assertEqual(int, type(response.json['tiendas'][0]['id']))
        self.assertEqual(200, response.status_code)

    def test_post_producto(self):
        # Given
        payload = json.dumps({
        "nombre": "chocosorpresa",
        "marca": "Ricolino"
        })
        # When
        response = self.app.post('/producto', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['sku']))
        self.assertEqual(200, response.status_code)

    def test_get_tienda(self):

        # When
        response = self.app.get('/producto')

        # Then
        self.assertEqual(str, type(response.json['productos'][0]['sku']))
        self.assertEqual(200, response.status_code)

    def test_post_inventario(self):
        # Given
        payload = json.dumps({
        "tienda_id":1,
        "producto_sku":"SKU000001",
        "cantidad":2,
        "valor_unitario":20.00
        })
        # When
        response = self.app.post('/inventario', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['producto_sku']))
        self.assertEqual(int, type(response.json['tienda_id']))
        self.assertEqual(200, response.status_code)

    def test_get_alerta(self):
        # When
        response = self.app.get('/alerta')
        # Then
        self.assertEqual(dict, type(response.json['alertas']))
        self.assertEqual(200, response.status_code)

    # def test_del_propietario(self):
    #     payload = json.dumps({
    #     "id":1
    #     })
    #     # When
    #     response = self.app.delete('/propietario', headers={"Content-Type": "application/json"}, data=payload)
    #     # Then
    #     self.assertEqual(str, type(response.json['message']))
    #     self.assertEqual(200, response.status_code)
    #
    # def test_del_producto(self):
    #     payload = json.dumps({
    #     "sku":"SKU000001"
    #     })
    #     # When
    #     response = self.app.delete('/producto', headers={"Content-Type": "application/json"}, data=payload)
    #     # Then
    #     self.assertEqual(str, type(response.json['message']))
    #     self.assertEqual(200, response.status_code)


    def tearDown(self):
        # Delete Database collections after the test is complete
        for collection in self.db.list_collection_names():
            self.db.drop_collection(collection)

if __name__ == '__main__':
    unittest.main()
