import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
#import psycopg2

####### Declaraciones y Configuraciones ########
app = Flask(__name__)
api = Api(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///'+os.path.join(basedir, 'data.sqlite') #SQLITE
# app.config['SQLALCHEMY_DATABASE_URI']="postgresql://Foundry:Mexico2021@localhost:5432/cargamos"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['SECRET_KEY'] = 'MySecretKey'

db = SQLAlchemy(app)
Migrate(app, db)
from cargamos.resources import register_urls


register_urls()

from cargamos.resources.propietarios import propietarios_blueprint
from cargamos.resources.tiendas import tiendas_blueprint
from cargamos.resources.productos import productos_blueprint
from cargamos.resources.inventarios import inventarios_blueprint
from cargamos.resources.alertas import alertas_blueprint

app.register_blueprint(propietarios_blueprint, url_prefix='/propietario')
app.register_blueprint(tiendas_blueprint, url_prefix='/tienda')
app.register_blueprint(productos_blueprint, url_prefix='/producto')
app.register_blueprint(inventarios_blueprint, url_prefix='/inventario')
app.register_blueprint(alertas_blueprint, url_prefix='/alerta')
