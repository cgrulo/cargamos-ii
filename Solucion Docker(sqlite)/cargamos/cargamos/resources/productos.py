from flask import Blueprint
from flask_restful import Resource, Api, reqparse
from cargamos.models import Producto
from cargamos import db
from cargamos.resources.parameters import define_parameters
from cargamos.resources.observer import logObserver
import asyncio

productos_blueprint = Blueprint('productos', __name__)

####### Productos #######
class ProductosInfo(Resource):
    '''
    Clase con la que se defina el endpoint para el modelo Producto.
    Metodos validos:
    * GET
    * POST
    * DELETE

    Parametros y funcionalidad:
    * GET :
        - Sin parametros: Regresa todas los propietarios registradas
        - sku, nombre: Pueden ser ambas o una sola(la prioridad es de SKU).
                       Si tiene sku regresa el producto especificado, si tiene el nombre
                       regresa los productos que compartan nombre.

    * POST :
        - nombre, marca: Con estos parametros se da de alta un nuevo producto
    * DELETE :
        - sku: con ese parametro se borra el propietario

    '''
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.event = None

    def setEvent(self, event):
        self.event = event

    def get(self):
        self.event = 'get'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, nombre=str, sku=str)
        ############ Get Values #############
        resp = self.parser.parse_args()
        if resp['sku']:
            prods = Producto.query.filter_by(sku=resp['sku'])
        elif resp['nombre']:
            prods = Producto.query.filter_by(nombre=resp['nombre'])
        else:
            prods = Producto.query.all()
        return dict(productos=[prod.json() for prod in prods])

    def post(self):
        self.event = 'post'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, nombre=str, marca=str)
        ############ Get Values #############
        resp = self.parser.parse_args()

        prod = Producto(resp['nombre'], resp['marca'])
        db.session.add(prod)
        db.session.commit()
        return prod.json()

    def delete(self):
        self.event = 'delete'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, sku=str)
        ############ Get Values #############
        resp = self.parser.parse_args()
        if ['sku']:
            prod = Producto.query.filter_by(sku=resp['sku']).first()
            if prod:
                db.session.delete(prod)
                db.session.commit()
                return {"message": "Producto borrado"}
