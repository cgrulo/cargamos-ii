from flask import Blueprint
from flask_restful import Resource, Api, reqparse
from cargamos.models import Tienda
from cargamos import db
#from cargamos.resources.abstract import Subject, Observer
from cargamos.resources.parameters import define_parameters
from cargamos.resources.observer import logObserver
import asyncio

tiendas_blueprint = Blueprint('tiendas', __name__)


####### Tienda API #######
class TiendaInfo(Resource):
    '''
    Clase con la que se defina el endpoint para el modelo tienda.
    Metodos validos:
    * GET
    * POST

    Parametros y funcionalidad:
    * GET :
        - Sin parametros: Regresa todas las tiendas registradas
        - id y marca: Se puede incluir una o ambas.(Pero la prioridad la tiene id)
                      Cuando tiene marca, regresa todas las tiendas que pertenecen a esa
                      marca/franquicia y con id regresa la tienda especificada
    * POST :
        - marca, direccion, propietario_id: Con estos parametros se da de alta una
                                            nueva tienda.
    '''
    # Implementacion fallida de Observador
    # def registerObserver(self, observer):
    #     print('Registering a new observer')
    #     self._observers.append(observer)
    #
    # def removeObserver(self, observer):
    #     print('Removing observer')
    #     self._observers.remove(observer)
    #
    # def notify(self,event):
    #     for observer in self._observers:
    #         observer.update(self, event)
    def __init__(self):
        self._parser = reqparse.RequestParser()
        self.event = None
    def setEvent(self, event):
        self.event = event


    def get(self):
        self.event = 'get'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self._parser, marca=str, id=int)
        ############ Get Values #############
        resp = self._parser.parse_args()

        if resp['id']:
            tiendas = Tienda.query.filter_by(id=resp['id'])
        elif resp['marca']:
            tiendas = Tienda.query.filter_by(marca=resp['marca'])
        else:
            tiendas = Tienda.query.all()

        return dict(tiendas=[tienda.json() for tienda in tiendas])


    def post(self):
        self.event = 'post'
        asyncio.run(logObserver(self, self.event))
        ########### Arguments #################
        define_parameters(self._parser, marca=str, direccion=str, propietario_id=str)
        ############ Get Values #############
        resp = self._parser.parse_args()

        tienda = Tienda(resp['marca'], resp['direccion'], resp['propietario_id'])
        db.session.add(tienda)
        db.session.commit()

        return tienda.json()
