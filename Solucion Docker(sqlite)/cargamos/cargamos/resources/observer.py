#from cargamos.resources.abstract import Observer
import logging
from datetime import datetime
import asyncio

# Configuracion para el log
#logging.basicConfig(filename='observer.log', level=logging.INFO)


# Definicion de las funciones ascinconas para el registro en el log
async def update(subject, event):

    #logging.info(f"&&&&&&&&&&&&&&&&&  Se registro un evento {event} en el sistema &&&&&&&&&&&&&&&&&")
    subject.setEvent(event=None)


async def logObserver(subject,event):
    await asyncio.gather(update(subject,event))


#   Definicion fallida de observer
#
# class logObserver(Observer):
#     events = {
#               'get':'info',
#               'post':'info',
#               'put':'info',
#               'delete':'warning'
#              }
#     def update(self, subject, event):
#         time = str(datetime().now())
#         if self.events[event]=='info':
#             logging.info(f"{time} Se registro un evento {event} en el sistema")
#             subject.setEvent(event=None)
#         elif self.events[event]=='warning':
#             logging.warnings(f"{time} Se registro un evento {event} en el sistema")
#             subject.setEvent(event=None)
#
# log = logObserver()
