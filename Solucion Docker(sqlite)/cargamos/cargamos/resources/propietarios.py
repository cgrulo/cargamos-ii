from flask import Blueprint
from flask_restful import Resource, Api, reqparse
from cargamos.models import Propietario
from cargamos import db
from cargamos.resources.parameters import define_parameters
from cargamos.resources.observer import logObserver
import asyncio


propietarios_blueprint = Blueprint('proietarios', __name__)

##### Propietarios######

class PropietarioInfo(Resource):
    '''
    Clase con la que se defina el endpoint para el modelo Propietario.
    Metodos validos:
    * GET
    * POST
    * DELETE

    Parametros y funcionalidad:
    * GET :
        - Sin parametros: Regresa todas los propietarios registradas
        - nombre: Regresa los  datos de propietarios que tienen el nombre especificado

    * POST :
        - nombre, rfc: Con estos parametros se da de alta un nuevo propietario

    *DELETE :
        - id: Con este parametro de borra el objeto seleccionado
    '''
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.event = None

    def setEvent(self, event):
        self.event = event

    def get(self):
        self.event = 'get'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, nombre=str)
        ############ Get Values #############
        resp = self.parser.parse_args()

        # If sent name you will get the products with that name
        # otherwise you will get all the products

        if resp['nombre']:
            try:
                props = Propietario.query.filter_by(nombre=resp['nombre'])
                return dict(propietarios=[prop.json() for prop in props])
            except:
                return {'message':'error'}
        else:
            props = Propietario.query.all()
            return dict(propietarios=[prop.json() for prop in props])

    def post(self):
        self.event = 'post'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, nombre=str, rfc=str)
        ############ Get Values #############
        resp = self.parser.parse_args()
        # Create model
        prop = Propietario(resp['nombre'], resp['rfc'])
        db.session.add(prop)
        db.session.commit()
        return prop.json()

    def delete(self):
        self.event = 'delete'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, id=int)
        ############ Get Values #############
        resp = self.parser.parse_args()
        if resp['id']:

            prop = Propietario.query.filter_by(id=resp['id']).first()
            if prop:
                db.session.delete(prop)
                db.session.commit()
                return {"message": "Propietario borrado"}
