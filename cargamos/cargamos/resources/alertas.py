from flask import Blueprint
from flask_restful import Resource, Api, reqparse
from cargamos import db
from cargamos.models import Inventario, Tienda, Producto
from cargamos.resources.parameters import define_parameters
from cargamos.resources.observer import logObserver
import asyncio

alertas_blueprint = Blueprint('alertas', __name__)

class AlertaInfo(Resource):
    '''
    Clase con la que se defina el endpoint para revisar las alarmas.
    Metodos validos:

    * GET

    Parametros y funcionalidad:
    * GET :
        -sin parametros: Regresa todas las alertas.
        - marca: Se regresan las alertas de falta de inventario por para las
        marcas/franquicias

    '''
    def __init__(self):
        self._parser = reqparse.RequestParser()
        self.event = None

    def setEvent(self, event):
        self.event = event

    parser = reqparse.RequestParser()
    def get(self):
        self.event = 'get'
        asyncio.run(logObserver(self, self.event))
        ########### Arguments #################
        define_parameters(self._parser, marca=str)
        ############ Get Values #############
        resp = self._parser.parse_args()
        if resp['marca']:
            alertas = [[inventario.tienda_id, inventario.producto_sku] for inventario in Inventario.query.filter_by(alerta=True)]
            tiendas, productos = [alerta[0] for alerta in alertas],[alerta[1] for alerta in alertas]
            #productos_alertas = [inventario.producto_sku for inventario in Inventario.query.filter_by(alerta=True)]
            tiendas_marca = [Tienda.query.filter_by(id=id, marca=resp['marca']).first() for id in tiendas]
            productos_nombre_marca = [{prod.sku:[prod.nombre,prod.marca]} for prod in Producto.query.filter(Producto.sku.in_(productos))]
            print('productos: ',productos_nombre_marca)
            print('tiendas: ',tiendas_marca)
            return dict(productos=productos_nombre_marca, tiendas=[f'{tienda.marca} {tienda.direccion}' for tienda in tiendas_marca])
        else:
            alertas_id = [[inventario.tienda_id, inventario.producto_sku] for inventario in Inventario.query.filter_by(alerta=True)]
            alerta_datos = [[Tienda.query.filter_by(id=alerta[0]).first(),Producto.query.filter_by(sku=alerta[1]).first()] for alerta in alertas_id]
            alertas_mensaje = [f'{alerta[0].marca}-{alerta[0].direccion} necesita {alerta[1].nombre}-{alerta[1].marca}' for alerta in alerta_datos]
            return {
            'alertas':{
            'operativas':alertas_id,
            'humanos': alertas_mensaje
            }
            }
