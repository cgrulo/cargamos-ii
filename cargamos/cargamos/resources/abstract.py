# Documento donde se declaraban las clases abstractas
#  Fallido

from abc import ABC, abstractmethod

class Observer(ABC):

    @abstractmethod
    def update(self, subject, event) -> None:
        pass

class Subject(ABC):

    @abstractmethod
    def registerObserver(self, observer:Observer) -> None:
        pass

    @abstractmethod
    def removeObserver(self, observer:Observer) -> None:
        pass

    @abstractmethod
    def notify(self, event) ->None:
        pass
