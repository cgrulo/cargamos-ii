from flask_restful import reqparse


def define_parameters(parser,**kwargs):
    # This part will receive dictionary with {'name':'type'}
    for arg in kwargs.items():
        parser.add_argument(arg[0], type=arg[1])
    return parser
