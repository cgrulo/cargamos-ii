from cargamos import api, app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
# from cargamos import db
db = SQLAlchemy(app)

from cargamos.resources.inventarios import InventarioInfo
from cargamos.resources.productos import ProductosInfo
from cargamos.resources.propietarios import PropietarioInfo
from cargamos.resources.tiendas import TiendaInfo
from cargamos.resources.alertas import AlertaInfo



def register_urls():
    api.add_resource(PropietarioInfo, '/propietario')
    api.add_resource(ProductosInfo, '/producto')
    api.add_resource(TiendaInfo, '/tienda')
    api.add_resource(InventarioInfo, '/inventario')
    api.add_resource(AlertaInfo, '/alerta')
