from flask import Blueprint
from flask_restful import Resource, Api, reqparse
from cargamos.models import Producto, Inventario, Tienda
from cargamos import db
from datetime import datetime
from cargamos.resources.parameters import define_parameters
from cargamos.resources.observer import logObserver
import asyncio

inventarios_blueprint = Blueprint('inventarios', __name__)


######### Inventario API #################
class InventarioInfo(Resource):
    '''
    Clase con la que se defina el endpoint para el modelo Inventario.
    Metodos validos:

    * POST
    * PUT

    Parametros y funcionalidad:
    * POST :
        - tienda_id, producto_sku,cantidad, valor_unitario: Con estos parametros
        se vinculan las tiendas y los productos con una cantidad de productos y
        el valor que puede cambiar segun el tendero o la marca/franquicia de la tienda.


    * PUT :
        - tienda_id, producto_sku,cantidad, valor_unitario: Se puede actualizar los
        inventarios completamente

    '''
    # def get(self):
    #
    #     inventarios = Inventario.query.all()
    #     return dict(inventarios=[inventario.json() for inventario in inventarios])
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.event = None

    def setEvent(self, event):
        self.event = event

    def validate_sku(self, sku):
        # This function validates if the SKU exist
        prod = Producto.query.filter_by(sku=sku).first()
        if prod:
            return sku
    def validate_store(self,id):
        tienda = Tienda.query.filter_by(id=id).first()
        print(str(tienda))
        if tienda:
            return id

    def post(self):
        self.event = 'post'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, tienda_id=int, producto_sku=str, cantidad=int, valor_unitario=float)
        ############ Get Values #############
        resp = self.parser.parse_args()

        if resp['producto_sku'] and resp['tienda_id']:
            inventario = Inventario(resp['tienda_id'], resp['producto_sku'], resp['cantidad'], resp['valor_unitario'])
            db.session.add(inventario)
            db.session.commit()
            return inventario.json()
        else:
            return {'message': 'Numero de SKU o id de tienda no valido'}

    def put(self):
        self.event = 'put'
        asyncio.run(logObserver(self,self.event))
        ########### Arguments #################
        define_parameters(self.parser, tienda_id=int, producto_sku=str, cantidad=int, valor_unitario=float)
        ############ Get Values #############
        resp = self.parser.parse_args()

        if resp['tienda_id'] and resp['producto_sku']:
            #inventario = Inventario.query.filter_by(tienda_id=tienda_id, producto_sku=producto_sku).first()
            inventario = db.session.query(Inventario).filter_by(tienda_id=resp['tienda_id'], producto_sku=resp['producto_sku']).first()
            if inventario:
                if resp['cantidad']:
                    inventario.cantidad = resp['cantidad']
                if resp['valor_unitario']:
                    inventario.valor_unitario = resp['valor_unitario']
                db.session.commit()
                tienda = db.session.query(Tienda).get(resp['tienda_id'])
                tienda.ultimo_pedido = datetime.now()
                db.session.commit()
                return {"message":"Actualizacion correcta",
                "actualizaciones":{
                "tienda":resp['tienda_id'],
                "producto":resp['producto_sku'],
                "cantidad":"Sin cambios" if not resp['cantidad'] else resp['cantidad'],
                "valor_unitario": "Sin cambios" if not resp['valor_unitario'] else resp['valor_unitario']
                }
                }
            else:
                return {"message":"Revisa que tengas uns SKU y id de tienda validos"}
