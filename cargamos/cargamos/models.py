from cargamos import db
from cargamos.resources.abstract import Observer
##############################################################
################### Model Propietario ########################
##############################################################
class Propietario(db.Model):
    '''
    --------------- Propietario ------------------
    Propietario es una entidad que esta relacionado con la entidad
    Tienda mediante una relacion 1:N con la cual permite que un
    propietario tenga caulquier cantidad de tiendas.
    '''
    __tablename__ = 'propietarios'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(250))
    rfc = db.Column(db.String(20))
    tiendas_id = db.relationship('Tienda', backref='propietario', lazy='dynamic', cascade="all,delete")

    def __init__(self, nombre, rfc):
        self.nombre = nombre
        self.rfc = rfc

    # def tiendas(self):
    #     return self.tiendas_id
    def json(self):
        
        return {
        'id':self.id,
        'nombre': self.nombre,
        'rfc':self.rfc,
        'tiendas':[tienda.__str__() for tienda in self.tiendas_id.all()]
        }


##############################################################
################### Model Tienda #############################
##############################################################
class Tienda(db.Model):
    '''
    --------------- Tienda ------------------
    Tienda es una entidad que esta relacionada a propietario y
    a inventario, con inventario tiene una relacion de 1:N permitiendo
    que se tenga cualquier cantidad de productos en su inventario
    '''
    __tablename__ = 'tiendas'
    id = db.Column(db.Integer, primary_key=True)
    marca = db.Column(db.String(120))
    direccion = db.Column(db.Text)
    propietario_id = db.Column(db.Integer, db.ForeignKey('propietarios.id'))
    ultimo_pedido = db.Column(db.DateTime())
    inventario_id = db.relationship('Inventario', backref='propietario', lazy='dynamic',cascade="all,delete")

    def __init__(self, marca, direccion, propietario):
        self.marca = marca
        self.direccion = direccion
        self.propietario_id = propietario

    def json(self):
        return {
        'id':self.id,
        'marca': self.marca,
        'direccion':self.direccion,
        'propietario_id':self.propietario_id,
        'ultimo_pedido':str(self.ultimo_pedido),
        'inventario':[inventario.__str__() for inventario in self.inventario_id.all()]
        }
    def __str__(self):
        return f'id {self.id}: {self.marca} en {self.direccion}'


##############################################################
################### Model Producto ###########################
##############################################################

class Producto(db.Model):
    '''
    --------------- Producto ------------------
    Producto es una entidad que incluye los datos de los productos con un
    SKU autogenerado consecutivo, esta entidad tiene una relacion de 1:N con
    la entidad Inventario permitiendo vincular el producto a una tienda.
    '''
    __tablename__='productos'
    sku = db.Column(db.String(20), primary_key=True)
    nombre = db.Column(db.String(100))
    marca = db.Column(db.String(100))
    inventario_id = db.relationship('Inventario', backref='producto', lazy='dynamic', cascade="all,delete")

    def generate_sku(self):
        try:
            #getting las SKU
            last_sku = Producto.query.order_by(Producto.sku.desc()).first().sku
            #generate the next SKU
            sku = 'SKU'+str(int(last_sku.replace('SKU',''))+1).zfill(6)
        except AttributeError:
            sku = 'SKU000001'
        return sku

    def __init__(self, nombre, marca, precio=None):
        self.sku = self.generate_sku()
        self.nombre = nombre
        self.marca = marca
        self.generate_sku()
        #self.precio = precio

    def json(self):
        return {
        'sku':self.sku,
        'nombre':self.nombre,
        'marca':self.marca,
        #'precio':self.precio
        }


##############################################################
################### Model Inventario #########################
##############################################################
class Inventario(db.Model):
    '''
    --------------- Inventario ------------------
    Inventario es una entidad cuya finalida es vincular productos
    con tiendas, por medio de sus llaves foraneas, ademas de incluir
    informacion relevante acerca de cada producto, como la cantidad
    y el precio unitario. Pensando en que el precio unitario puede
    depender de cada tendero, se incluyo este dato en ese apartado y no en
    producto directamente.

    * Alerta de poco inventario:
    Ademas de esto la entidad inventario tiene un metodo con el
    que se genera una alerta al tener 5 o menos productos en el inventario.
    '''
    __tablename__ = 'inventarios'
    __table_args__ = (
        db.PrimaryKeyConstraint('tienda_id', 'producto_sku'),
    )
    tienda_id = db.Column(db.Integer, db.ForeignKey('tiendas.id'))
    producto_sku = db.Column(db.String(20), db.ForeignKey('productos.sku'))
    cantidad = db.Column(db.Integer)
    valor_unitario = db.Column(db.Numeric)
    alerta = db.Column(db.Boolean)

    def __init__(self, tienda_id, sku, cantidad, valor):
        self.tienda_id = tienda_id
        self.producto_sku = sku
        self.cantidad = cantidad
        self.valor_unitario = valor
        self.alerta = self.alerta_validate()

    def json(self):
        return {
        'tienda_id': self.tienda_id,
        'producto_sku': self.producto_sku,
        'cantidad': self.cantidad,
        'valor_unitario': str(self.valor_unitario),
        'alerta': self.alerta
        }
    def valor_total(self):
        return round(self.cantidad*self.valor_unitario,2)


    def alerta_validate(self):
        if self.cantidad <= 5:
            return True
        else:
            return False

    def __str__(self):
        if not self.alerta_validate():
            return f'{self.producto_sku} cantidad {self.cantidad} valor total: ${self.valor_total()}mxn'
        else:
            return f'¡ALERTA! Tienda: {self.tienda_id}, Producto: {self.producto_sku} cantidad {self.cantidad} valor total: ${self.valor_total()}mxn'
